#include <SoftwareSerial.h>

#define MYPORT_TX 13
#define NO_PORT -1

EspSoftwareSerial::UART mySerial;

void setup() {
  mySerial.begin(4800, SWSERIAL_8N1, NO_PORT, MYPORT_TX, false);
  if (!mySerial) { // If the object did not initialize, then its configuration is invalid
    Serial.println("Invalid EspSoftwareSerial pin configuration, check config");
    while (1) { // Don't continue with invalid configuration
      delay (1000);
    }
  } 
  
}

void loop() {
  mySerial.println("Hello");

  delay(2000);
}


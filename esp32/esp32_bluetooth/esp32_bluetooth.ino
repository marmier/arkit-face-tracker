/*
    Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleServer.cpp
    Ported to Arduino ESP32 by Evandro Copercini
    updates by chegewara
*/

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define X_CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"
#define Y_CHARACTERISTIC_UUID "ba7ff775-e5d6-4535-ae2d-70dfa683f250"
#define PEN_CHARACTERISTIC_UUID "25a6492e-f044-4021-8de0-3255f82a4545"

BLECharacteristic *xCharacteristic;
BLECharacteristic *yCharacteristic;
BLECharacteristic *penCharacteristic;

void setup() {
  Serial.begin(115200);
  Serial.println("Starting BLE work!");

  BLEDevice::init("FaceDoodle Plotter");
  BLEServer *pServer = BLEDevice::createServer();
  BLEService *pService = pServer->createService(SERVICE_UUID);
  xCharacteristic = pService->createCharacteristic(
    X_CHARACTERISTIC_UUID,
    BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);

  yCharacteristic = pService->createCharacteristic(
    Y_CHARACTERISTIC_UUID,
    BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);

  penCharacteristic = pService->createCharacteristic(
    PEN_CHARACTERISTIC_UUID,
    BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);



  xCharacteristic->setValue("0");
  yCharacteristic->setValue("0");
  penCharacteristic->setValue("false");
  pService->start();
  // BLEAdvertising *pAdvertising = pServer->getAdvertising();  // this still is working for backward compatibility
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  BLEDevice::startAdvertising();
  Serial.println("Characteristics defined! Now you can read it in your phone!");
}

void loop() {
  // put your main code here, to run repeatedly:
  uint8_t *X_val = xCharacteristic->getData();
  uint8_t *Y_val = yCharacteristic->getData();
  uint8_t *P_val = penCharacteristic->getData();
  
  String *sX = new String((char *) X_val);
  String *sY = new String((char *) Y_val);
  String *sP = new String((char *) P_val);

  Serial.print("x is : " + *sX + " ");
  Serial.print("y is : " + *sY + " ");
  Serial.println("pen is : " + *sP);


  delay(100);
}

#include <Servo.h>



Servo myservo;  // create servo object to control a servo

int val;    // variable to read the value from the analog pin
boolean isEnabled = false; // value sent by bluetooth
void setup() {
  myservo.attach(9);  // attaches the servo on pin 10 to the servo object
}

void loop() {
  if(isEnabled) 
  {
    val = 90;
  } else {
    val = 180;
  }
  myservo.write(val);                  // sets the servo position 
  delay(8000);                         // waits for the servo to get there
 // isEnabled = !isEnabled;
}
